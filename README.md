We at Collared Greens are proud to be the first and only American made, eco-conscious lifestyle clothing brand. The Collared Greens brand combines our passion for timeless style with our commitment to support American manufacturing and the environment around us. We are always American Made, and continuously strive to deliver on the promise of premium quality American craftsmanship, creativity, and conservation.

Our signature array of mens clothing and accessories are timeless and classic with a nod to modern style and comfort. Thank you for your support of Collared Greens and the American Made movement.

Live American Made.
